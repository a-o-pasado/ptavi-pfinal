#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""


import sys
import simplertp
import secrets
import socket
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from proxy_registrar import get_digest_response, LOG

class SmallSMILHandler(ContentHandler):

    def __init__(self):
        self.elementos = []

        self.tags = ['account', 'uaserver', 'rtpaudio', 'regproxy', 'log', 'audio']
        self.atributos = {
            'account': ['username', 'passwd'],
            'uaserver': ['ip', 'puerto'],
            'rtpaudio': ['puerto'],
            'regproxy': ['ip', 'puerto'],
            'log': ['path'],
            'audio': ['path']
        }

    def startElement(self, name, attrs):
        diccionario = {}

        if name in self.tags:

            diccionario[name] = {}
            for atributo in self.atributos[name]:
                diccionario[name][atributo] = attrs.get(atributo, "")
            self.elementos.append(diccionario)

    def get_tags(self):
        return self.elementos

if __name__ == "__main__":
    usage_error = "$ python3 uaclient.py config metodo opcion"
    if len(sys.argv) != 4:
        sys.exit(usage_error)
    else:
        config = sys.argv[1]
        method = sys.argv[2]
        opcion = sys.argv[3]

    parser = make_parser()
    SmallSMILHandler = SmallSMILHandler()
    parser.setContentHandler(SmallSMILHandler)
    parser.parse(open(config))
    tags = SmallSMILHandler.get_tags()
    log = LOG(tags[4]["log"]["path"])
    print(tags)

    if method == "REGISTER":
        message = "REGISTER sip:"+tags[0]["account"]["username"]+":"+tags[1]["uaserver"]["puerto"]+" SIP/2.0\r\nExpires: "+opcion
        ip_proxy = tags[3]["regproxy"]["ip"]
        puerto_proxy = int(tags[3]["regproxy"]["puerto"])
        address_proxy = ip_proxy + ":" + str(puerto_proxy)
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((ip_proxy, puerto_proxy))
            log.sent_to(address_proxy, message)
            my_socket.send(bytes(message +"\r\n", "utf-8"))
            data = my_socket.recv(1024).decode("utf-8")
            log.received_from(address_proxy, data)
            print(data)
            if "401 Unauthorized" in data:
                nonce = data.split("\r\n")[1].split("\"")[1]
                passwd = tags[0]["account"]["passwd"]
                response = get_digest_response(nonce, passwd)
                message += "\r\nAuthorization: Digest response=\""+response+"\""
                log.sent_to(address_proxy, message)
                my_socket.send(bytes(message + "\r\n", "utf-8"))
                data = my_socket.recv(1024).decode("utf-8")
                log.received_from(address_proxy, data)
                print(data)
    elif method == "INVITE":
        message = "INVITE sip:"+opcion+" SIP/2.0\
            \r\nContent-Type: application/sdp\
            \r\nContent-Length: 76\
            \r\n\r\nv=0\
            \r\no="+tags[0]["account"]["username"]+" "+tags[1]["uaserver"]["ip"]+"\
            \r\ns=misesion\
            \r\nt=0\
            \r\nm=audio "+tags[2]["rtpaudio"]["puerto"]+" RTP\r\n"
        ip_proxy = tags[3]["regproxy"]["ip"]
        puerto_proxy = int(tags[3]["regproxy"]["puerto"])
        address_proxy = ip_proxy + ":" + str(puerto_proxy)
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((ip_proxy, puerto_proxy))
            log.sent_to(address_proxy, message)
            my_socket.send(bytes(message, "utf-8"))
            data = my_socket.recv(1024).decode("utf-8")
            log.received_from(address_proxy, data)
            print(data)
            if "100 Trying" in data and "180 Ringing" in data and "200 OK":
                message = "ACK sip:"+opcion+" SIP/2.0\r\n"
                log.sent_to(address_proxy, message)
                my_socket.send(bytes(message, "utf-8"))
                bit = secrets.randbelow(2)
                aleat = secrets.randbelow(150)

                RTP_header = simplertp.RtpHeader()
                RTP_header.set_header(marker=bit, ssrc=aleat)
                audio = simplertp.RtpPayloadMp3(tags[5]["audio"]["path"])
                simplertp.send_rtp_packet(RTP_header, audio, data.split("\r\n")[9].split()[-1], int(data.split("\r\n")[-2].split()[1]))
    elif method == "BYE":
        message = "BYE sip:"+opcion+" SIP/2.0\r\n"
        ip_proxy = tags[3]["regproxy"]["ip"]
        puerto_proxy = int(tags[3]["regproxy"]["puerto"])
        address_proxy = ip_proxy + ":" + str(puerto_proxy)
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((ip_proxy, puerto_proxy))
            log.sent_to(address_proxy, message)
            my_socket.send(bytes(message +"\r\n", "utf-8"))
            data = my_socket.recv(1024).decode("utf-8")
            log.received_from(address_proxy, data)
            print(data)