#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import os
import sys
import json
import hashlib
import time
import socket
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import secrets
date_log ="%Y%m%d%H%M%S"

class LOG:

    def __init__(self, file_name):
        if not os.path.exists(file_name):
            os.system('touch ' + file_name)
        self.file = file_name

    def starting(self):
        now = time.gmtime(time.time() + 3600)
        log = open(self.file, 'a')
        log.write(time.strftime((date_log), now) + ' Starting...\n')
        log.close()

    def error(self, message):
        now = time.gmtime(time.time() + 3600)
        error_message = ' Error: ' + message + '\n'
        log = open(self.file, 'a')
        log.write(time.strftime((date_log), now) + error_message)
        log.close()

    def sent_to(self, address, message):
        line = ''
        for part in message.split('\r\n'):
            if part != '':
                line += part
            line += ' '
        line += '\n'
        now = time.gmtime(time.time() + 3600)
        sent_message = ' Sent to: ' + address + ': ' + line
        log = open(self.file, 'a')
        log.write(time.strftime((date_log), now) + sent_message)
        log.close()

    def received_from(self, address, message):
        line = ''
        for part in message.split('\r\n'):
            if part != '':
                line += part
            line += ' '
        line += '\n'
        now = time.gmtime(time.time() + 3600)
        received_message = ' Received from: ' + address + ' ' + line
        log = open(self.file, 'a')
        log.write(time.strftime((date_log), now) + received_message)
        log.close()

    def finishing(self):
        now = time.gmtime(time.time() + 3600)
        log = open(self.file, 'a')
        log.write(time.strftime((date_log), now) + ' Finishing\n')
        log.close()

def get_digest_response(nonce, passwd):

    digest = hashlib.md5()
    digest.update(bytes(nonce, "utf-8"))
    digest.update(bytes(passwd, "utf-8"))
    digest.digest()
    return digest.hexdigest()

class ProxyHandler(socketserver.DatagramRequestHandler):

    regusers = {}
    passwd = {}
    nonce= {}

    def get_time_expire(self, exp_line):
        exp = int(exp_line.split()[1])
        now = time.gmtime(time.time()+exp)
        return time.strftime(("%d/%m/%Y %H:%M:%S"), now)

    def json2pawssd(self):
        try:
            with open(tags[1]["database"]["passwdpath"], "r") as jsonfile:
                self.passwd = json.load(jsonfile)
        except:

            pass

    def json2register(self):

        try:
            with open(tags[1]["database"]["path"], "r") as jsonfile:
                self.regusers = json.load(jsonfile)
        except:

            pass

    def register2json(self):
        with open(tags[1]["database"]["path"], "w") as jsonfile:
            json.dump(self.regusers, jsonfile, indent=3)

    def get_nonce(self, username):
        self.nonce[username] = secrets.token_hex(8)
        return self.nonce[username]

    def registrar(self, data):
        response = "SIP/2.0 200 OK\r\n"
        username = data.split("\r\n")[0].split()[1].split(":")[1]
        print(data)
        print(username)
        if username in self.regusers:
            exp = int(data.split("\r\n")[1].split()[1])
            print(exp)
            if exp == 0:
                del self.regusers[username]
            else:
                self.regusers[username]["expires"] = self.get_time_expire(data.split("\r\n")[1])
        else:
            if "Authorization" in data:
                digest = data.split("\r\n")[2].split("\"")[1]
                resp = get_digest_response(self.nonce[username], self.passwd[username])
                if digest == resp:
                    self.regusers[username] = {
                        "ip": self.client_address[0],
                        "puerto": data.split("\r\n")[0].split()[1].split(":")[2],
                        "expires": self.get_time_expire(data.split("\r\n")[1])
                    }
                else:
                    response = "SIP/2.0 404 User Not Found\r\n"
            else:
                nonce = self.get_nonce(username)
                response = "SIP/2.0 401 Unauthorized\r\nWWW Authenticate: Digest nonce=\"" + nonce + "\"\r\n"
        return response

    def invitar(self, data):
        response = "SIP/2.0 200 OK\r\n"
        user_src = data.split("\r\n")[5].split()[0].split("=")[1]
        print(user_src)
        if user_src in self.regusers:
            user_dst = data.split("\r\n")[0].split()[1].split(":")[1]
            print(user_dst)
            if user_dst in self.regusers:
                response = self.reenviar(user_dst, data)
            else:
                response = "SIP/2.0 404 User Not Found\r\n"
        else:
            response = "SIP/2.0 404 User Not Found\r\n"
        return response

    def finalizar(self,data):
        response = "SIP/2.0 200 OK\r\n"
        user_dst = data.split("\r\n")[0].split()[1].split(":")[1]
        print(user_dst)
        if user_dst in self.regusers:
            response = self.reenviar(user_dst, data)
        else:
            response = "SIP/2.0 404 User Not Found\r\n"

    def mensajes(self,data):
        response = "SIP/2.0 200 OK\r\n"
        user_dst = data.split("\r\n")[0].split()[1].split(":")[1]
        print(user_dst)
        if user_dst in self.regusers:
            response = self.reenviar(user_dst, data)
            response = None
        else:
            response = "SIP/2.0 404 User Not Found\r\n"

    def handle(self):
        self.json2register()
        self.json2pawssd()
        address = self.client_address[0]+":"+str(self.client_address[1])
        data = self.rfile.read().decode("utf-8")
        log.received_from(address, data)
        print(self.regusers)
        print(data)
        method = data.split("\r\n")[0].split()[0]
        print(method)
        response = "SIP/2.0 200 OK\r\n"
        if method == "REGISTER":
           response = self.registrar(data)
        elif method == "INVITE":
            response = self.invitar(data)
        elif method == "BYE":
            response = self.finalizar(data)
        elif method == "ACK":
            response = self.mensajes(data)
        else:
            response = "SIP/2.0 405 Method Not Allowed\r\n"
        self.register2json()
        if response:
            log.sent_to(address, response)
            self.wfile.write(bytes(response, "utf-8"))

    def reenviar(self, username, data):
        ip = self.regusers[username]["ip"]
        puerto = int(self.regusers[username]["puerto"])
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            my_socket.connect((ip, puerto))
            my_socket.send(bytes(data, "utf-8"))
            log.sent_to(ip+":"+str(puerto), data)
            response = my_socket.recv(1024).decode("utf-8")
        return response


class SmallSMILHandler(ContentHandler):

    def __init__(self):
        self.elementos = []
        self.tags = ['server', 'database', 'log']
        self.atributos = {
            'server': ['name', 'ip', 'puerto'],
            'database': ['path', 'passwdpath'],
            'log': ['path']
            }

    def startElement(self, name, attrs):
        diccionario = {}
        if name in self.tags:
            diccionario[name] = {}
            for atributo in self.atributos[name]:
                diccionario[name][atributo] = attrs.get(atributo, "")
            self.elementos.append(diccionario)

    def get_tags(self):
        return self.elementos

if __name__ == "__main__":
    usage_error = "$ python3 proxy_registrar.py config"
    if len(sys.argv) != 2:
        sys.exit(usage_error)
    else:
        config = sys.argv[1]

    parser = make_parser()
    SmallSMILHandler = SmallSMILHandler()
    parser.setContentHandler(SmallSMILHandler)
    parser.parse(open(config))
    tags = SmallSMILHandler.get_tags()

    ip = tags[0]["server"]["ip"]
    puerto = int(tags[0]["server"]["puerto"])
    log = LOG(tags[2]["log"]["path"])
    proxy = socketserver.UDPServer((ip, puerto), ProxyHandler)
    try:
        log.starting()
        proxy.serve_forever()
    except:
        log.finishing()
        sys.exit("bye")



