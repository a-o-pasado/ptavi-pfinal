#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor de eco en UDP simple."""

import socketserver
import sys
import simplertp
from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from proxy_registrar import LOG
import secrets

class SmallSMILHandler(ContentHandler):

    def __init__(self):
        self.elementos = []
        self.tags = ['account', 'uaserver', 'rtpaudio', 'regproxy', 'log', 'audio']
        self.atributos = {
            'account': ['username', 'passwd'],
            'uaserver': ['ip', 'puerto'],
            'rtpaudio': ['puerto'],
            'regproxy': ['ip', 'puerto'],
            'log': ['path'],
            'audio': ['path']
        }

    def startElement(self, name, attrs):
        diccionario = {}
        if name in self.tags:

            diccionario[name] = {}
            for atributo in self.atributos[name]:
                diccionario[name][atributo] = attrs.get(atributo, "")
            self.elementos.append(diccionario)

    def get_tags(self):
        return self.elementos

class ServerHandler(socketserver.DatagramRequestHandler):
    rtp= []

    def invitar(self,data, address):
        response = "SIP/2.0 200 OK\r\n"
        ip = data.split("\r\n")[5].split()[-1]
        puerto = data.split("\r\n")[-2].split()[1]
        print(ip, puerto)
        self.rtp.append(ip)
        self.rtp.append(int(puerto))
        response = "SIP/2.0 100 Trying\r\n\r\n"
        response += "SIP/2.0 180 Ringing\r\n\r\n"
        response += "SIP/2.0 200 OK\r\n"
        response += "Content-Type: application/sdp\
\r\nContent-Length: 76\
\r\n\r\nv=0\
\r\no=" + tags[0]["account"]["username"] + " " + tags[1]["uaserver"]["ip"] + "\
\r\ns=misesion\
\r\nt=0\
\r\nm=audio " + tags[2]["rtpaudio"]["puerto"] + " RTP\r\n"
        log.sent_to(address, response)
        self.wfile.write(bytes(response, "utf-8"))

    def finalizar(self, data, address):
        response = "SIP/2.0 200 OK\r\n"
        log.sent_to(address, "SIP/2.0 200 OK\r\n")
        self.rtp = []
        self.wfile.write(bytes("SIP/2.0 200 OK\r\n", "utf-8"))

    def mensajes(self, data, address):
        response = "SIP/2.0 200 OK\r\n"
        bit = secrets.randbelow(2)
        aleat = secrets.randbelow(150)

        RTP_header = simplertp.RtpHeader()
        RTP_header.set_header(marker=bit, ssrc=aleat)
        audio = simplertp.RtpPayloadMp3(tags[5]["audio"]["path"])
        simplertp.send_rtp_packet(RTP_header, audio, self.rtp[0], self.rtp[1])

    def handle(self):
        address = self.client_address[0]+":"+str(self.client_address[1])
        data = self.rfile.read().decode("utf-8")
        log.received_from(address, data)
        print(data)
        method = data.split()[0]
        if method == "INVITE":

            response = self.invitar(data, address)

        elif method == "ACK":

            response = self.mensajes(data, address)

        elif method == "BYE":

            response = self.finalizar(data, address)



if __name__ == "__main__":
    usage_error = "$ python3 uaserver.py config"
    if len(sys.argv) != 2:
        sys.exit(usage_error)
    else:
        config = sys.argv[1]

    parser = make_parser()
    SmallSMILHandler = SmallSMILHandler()
    parser.setContentHandler(SmallSMILHandler)
    parser.parse(open(config))
    tags = SmallSMILHandler.get_tags()
    print(tags)
    ip = tags[1]["uaserver"]["ip"]
    puerto = int(tags[1]["uaserver"]["puerto"])
    log = LOG(tags[4]["log"]["path"])
    server = socketserver.UDPServer((ip, puerto), ServerHandler)
    try:
        log.starting()
        server.serve_forever()
    except:
        log.finishing()
        sys.exit("bye")
